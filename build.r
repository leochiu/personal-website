#!/usr/bin/Rscript
if(require('blogdown')==FALSE){install.packages('blogdown')}
library('blogdown')
blogdown::install_hugo()
blogdown::build_site(build_rmd = 'timestamp')
