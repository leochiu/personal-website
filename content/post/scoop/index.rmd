---
title: Installing applications for windows easily with scoop
author: Leo CHIU
date: '`r Sys.Date()`'
---

# Motivation
We sometimes need to install a large number of applications at once, and it can be quite annoying to
go to the website of each of the software one by one for installing them. Also, a common problem we
encounter is that it is difficult and time consuming to track whether any of the applications need
update and we need to update the application one by one. These motivate us to use an installer to
handle the installation instead of doing it manually where scoop is one of the installers.

# Scoop
## Quick start
Refer to this link: <https://github.com/ScoopInstaller/Scoop/wiki/Quick-Start>

## Features
Refer to this link: <https://github.com/ScoopInstaller/Scoop#what-does-scoop-do>.
Basically, we mainly install [portable applications](https://en.wikipedia.org/wiki/Portable_application) through scoop and thus there are various advantages which obviously include 'portable'.

## Typical workflow
### Search applications available in scoop
I recommend using [shovel](https://shovel.sh) to search the applications or install [scoop-search](https://github.com/shilangyu/scoop-search) and use the command `scoop-search` as they are faster than the command `scoop search` 

### Install applications in scoop
Run 
```
scoop install application-name
```
and run 
```
scoop bucket add bucket-name
```
when needed^[Change the name to the name of application found in [shovel](https://shovel.sh)].

### Update all outdated applications at once
Run 
```
scoop update *
```
to update all applications.

## Problems
Some applications may not be available or may be outdated in scoop. 

## Solutions
- Open an issue in appropriate bucket for the apps as suggested [here](https://github.com/ScoopInstaller/Scoop#known-application-buckets)
- Use alternative way to install those applications, e.g. manually
