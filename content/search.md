---
title: "Search" # in any language you want
layout: "search" # is necessary
# url: "/archive"
description: "                   
- Arrow keys to move up/down the list
- Enter key (return) or Right Arrow key to Go to highlighted page
- Escape key to clear searchbox and results"
summary: "search"
placeholder: "input here"
---
