# Suggest changes for the posts
There are multiple ways.

1. Go to [here](https://codeberg.org/leochiu/pages/src/branch/main/content/post) and open the folder containing 
the post, then edit the `index.rmd` file and create a pull request.
1. Create an issue using [this link](https://codeberg.org/leochiu/pages/issues/new?title=Suggestion+about&body=Link+of+the+post:)
and you can make suggestions in the issue.
1. Contact me directly. Follow the instructions [here](https://leochiu.codeberg.page/contact/).

