![Website status](https://img.shields.io/website?down_color=red&down_message=down&up_message=up&url=https%3A%2F%2Fleochiu.codeberg.page)
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC_BY--SA_4.0-blue.svg)](https://creativecommons.org/licenses/by-sa/4.0/)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](../../../pulls)
[![Issues welcome](https://img.shields.io/badge/Issues-welcome-orange)](../../../issues)
# Introduction
This website is mainly powered by [Hugo](https://gohugo.io/) and [Blogdown](https://bookdown.org/yihui/blogdown/). 
The math typesetting is powered by [KaTeX](https://katex.org).

Link: <https://leochiu.codeberg.page>

# Offline reading
You can build the website from source by running the following commands:

```
git clone https://codeberg.org/leochiu/pages.git
cd pages
git checkout main
Rscript build-and-serve.r
```

and then you can view the website served by following the instructions.
The files built are contained in `public` folder (which are the files in pages branch).
